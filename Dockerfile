FROM python:3.7-alpine

RUN mkdir /srv/ludoparser
COPY . /srv/ludoparser
WORKDIR /srv/ludoparser
RUN apk add build-base libffi-dev libxml2-dev libxslt-dev openssl-dev
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "parser.py"]
