import logging
import os

from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings

from ludoparser import ParserRunner
from ludoparser.spiders.fansport import FansportSpider

if __name__ == '__main__':
    configure_logging(get_project_settings(), True)
    logging.info('Ludoparser: Starting up Ludoparser...')
    runner = ParserRunner()
    mode = os.environ['LUDOPARSER_MODE'] if 'LUDOPARSER_MODE' in os.environ else 'combined'
    logging.info(f'Ludoparser: Running in {mode} mode')
    if mode == 'combined':
        logging.warning('Ludoparser: COMBINED MODE IS MEANT ONLY FOR DEBUG PURPOSES! ' +
                        'DO NOT USE IT FOR FULL-FLEDGED TESTING OR PRODUCTION BUILDS!')

    if mode == 'live' or mode == 'combined':
        runner.add_spider(FansportSpider,
                          item_defs=['https://fan-sport.com/LiveFeed/Get1x2_VZip?count=100000&lng=en&mode=4'],
                          base_event_url='https://fan-sport.com/LiveFeed/GetGameZip?id={}&lng=en&cfview=0&countevents=999&groupEvents=true',
                          live=True)

    if mode == 'prematch' or mode == 'combined':
        runner.add_spider(FansportSpider,
                          item_defs=['https://fan-sport.com/LineFeed/Get1x2_VZip?count=100000&lng=en&mode=4'],
                          base_event_url='https://fan-sport.com/LineFeed/GetGameZip?id={}&lng=en&cfview=0&countevents=999&groupEvents=true',
                          live=False)

    logging.info('Ludoparser: Started!')
    runner.start()
