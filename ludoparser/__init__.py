import csv
import json
import logging
import pika

from pika.exceptions import AMQPError
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

settings = get_project_settings()


class SportSelector:
    _sports: dict = None

    @staticmethod
    def get(item):
        try:
            return SportSelector._sports[item]
        except KeyError:
            return 'Undefined'


class ParserRunner:
    def __init__(self):
        self._process = CrawlerProcess(get_project_settings())
        self._spider_kwargs = dict()

    def start(self):
        self._process.start()

    def add_spider(self, spider, **kwargs):
        self._spider_kwargs[spider.name] = kwargs
        logging.info(f'{self.__class__.__name__}: {spider.name} spider loaded!')
        self._crawl_loop(None, spider)

    def _crawl_loop(self, result, spider):
        deferred_crawl = self._process.crawl(spider, **self._spider_kwargs[spider.name])
        deferred_crawl.addCallback(self._crawl_loop, spider)
        logging.info(f'{self.__class__.__name__}: Called {spider.name} spider')
        return deferred_crawl


class RMQPublisher:
    def __init__(self):
        self.channel = self._open_channel()
        self.channel.exchange_declare(exchange=settings.get('RMQ_EXCHANGE'), exchange_type='fanout')

    def send(self, item):
        sent = False
        while not sent:
            try:
                self.channel.basic_publish(exchange=settings.get('RMQ_EXCHANGE'), routing_key='',
                                           body=json.dumps(item))
                sent = True
            except AMQPError:
                print('Connection broke')
                self.channel = self._open_channel()

    @staticmethod
    def _open_channel():
        credentials = pika.PlainCredentials(settings.get('RMQ_USERNAME'), settings.get('RMQ_PASSWORD'))
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=settings.get('RMQ_HOST'),
                                                                       port=settings.get('RMQ_PORT'),
                                                                       credentials=credentials))

        channel = connection.channel()
        return channel


with open('misc/sports.csv', 'r') as _sports_file:
    _sports_reader = csv.reader(_sports_file)
    next(_sports_reader)
    SportSelector._sports = {int(row[0]): row[1] for row in _sports_reader}

rmq_publisher = RMQPublisher()
