from ludoparser import rmq_publisher
from ludoparser.items import Event, EventData
from scrapy.utils.project import get_project_settings

settings = get_project_settings()


class LudoparserPipeline(object):

    _item_types = {
        Event: 0,
        EventData: 1
    }

    def process_item(self, item, spider):
        message = {
            'headers': {
                'type': LudoparserPipeline._get_item_type(item),
                'provider': spider.name,
                'live': 1 if spider.live else 0
            },
            'body': dict(item)
        }

        rmq_publisher.send(message)
        return item

    @staticmethod
    def _get_item_type(item):
        return LudoparserPipeline._item_types[type(item)]
