import scrapy
from collections import namedtuple


class Sport(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()


class Participant(scrapy.Item):
    name = scrapy.Field()
    position = scrapy.Field()


class Bet(scrapy.Item):
    id = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()


class Score(scrapy.Item):
    position = scrapy.Field()
    amount = scrapy.Field()


class Event(scrapy.Item):
    id = scrapy.Field()
    sport = scrapy.Field()
    league = scrapy.Field()
    start_date = scrapy.Field()
    participants = scrapy.Field()
    last_update = scrapy.Field()


class Market(scrapy.Item):
    name = scrapy.Field()
    bets = scrapy.Field()


class Livescore(scrapy.Item):
    time = scrapy.Field()
    scoreboard = scrapy.Field()


class EventData(scrapy.Item):
    event_id = scrapy.Field()
    markets = scrapy.Field()
    livescore = scrapy.Field()
    last_update = scrapy.Field()
