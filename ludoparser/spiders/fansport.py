import json
import logging

from typing import Tuple, List
from ludoparser.items import *
from ludoparser.spiders import BaseSpider, InconsistentDataError

EVENT_URL = 'https://fan-sport.com/LiveFeed/GetGameZip?id={}&lng=en&cfview=0&countevents=999&groupEvents=true'

with open('misc/fansport_bet_model.json', 'r') as _bet_model_file:
    _bet_models = json.load(_bet_model_file)

BET_MODELS = _bet_models['model']
BET_MODEL_GROUPS = _bet_models['model_group']


class FansportSpider(BaseSpider):
    name = 'fan-sport'
    sports = {8: 1, 14: 2, 10: 3, 138: 4, 15: 5, 56: 6, 7: 7, 39: 8, 12: 9, 5: 10, 9: 11, 32: 12, 3: 13, 105: 14,
              67: 15, 126: 16, 66: 17, 6: 18, 80: 19, 2: 20, 1: 21, 40: 22, 21: 23, 28: 24, 11: 25, 16: 26, 189: 27,
              17: 28, 13: 29, 202: 30, 29: 31, 30: 32, 4: 33}

    def __init__(self, item_defs: list, base_event_url: str = None, **kwargs):
        super().__init__(item_defs, base_url=None, base_event_url=base_event_url, pre_crawl=False, **kwargs)

    def split_response(self, response) -> list:
        return json.loads(response.body_as_unicode())['Value']

    def parse_event(self, chunk) -> Tuple[Event, str]:
        try:
            home_team = Participant(name=chunk['O1'], position=1)
            away_team = Participant(name=chunk['O2'], position=2)
        except KeyError:
            raise InconsistentDataError
        event = Event(id=chunk['I'],
                      sport=self.get_sport(chunk['SI']),
                      league=chunk['L'],
                      start_date=FansportSpider.timestamp_to_date_string(chunk['S']),
                      participants=[dict(home_team), dict(away_team)],
                      last_update=FansportSpider.get_last_update())

        return event, chunk['I']

    def parse_data(self, response) -> EventData:
        try:
            response_data = json.loads(response.body_as_unicode())['Value']
            if response_data:
                event_id = response_data['I']

                try:
                    home_team = response_data['O1']
                    away_team = response_data['O2']
                except KeyError:
                    raise InconsistentDataError

                markets = []
                for market in response_data['GE']:
                    bets = []
                    for bet in market['E']:
                        for variant in bet:
                            variant_amount = variant['P'] if 'P' in variant else None
                            bet_name = BET_MODELS[str(variant['T'])]['N']
                            bet_name = bet_name.replace('^1^', home_team).replace('^2^', away_team)
                            bet_name = bet_name.replace('()', str(variant_amount))
                            bets.append(dict(Bet(id=f'{event_id}_{market["G"]}_{variant["T"]}',
                                                 name=bet_name,
                                                 price=variant['C'])))
                    try:
                        markets.append(dict(Market(name=BET_MODEL_GROUPS[str(market['G'])]['N'], bets=bets)))
                    except KeyError:
                        continue

                if self.live:
                    home_score_amount = response_data['SC']['FS']['S1'] if 'S1' in response_data['SC']['FS'] else 0
                    away_score_amount = response_data['SC']['FS']['S2'] if 'S2' in response_data['SC']['FS'] else 0

                    home_score = Score(position=1, amount=home_score_amount)
                    away_score = Score(position=1, amount=away_score_amount)

                    match_time = response_data['SC']['TS'] if 'TS' in response_data['SC'] else None
                    livescore = Livescore(time=match_time, scoreboard=[dict(home_score), dict(away_score)])
                else:
                    livescore = None

                event_data = EventData(event_id=event_id,
                                       markets=markets,
                                       livescore=dict(livescore) if livescore else None,
                                       last_update=FansportSpider.get_last_update())

                yield event_data
            else:
                logging.warning(f'{self.__class__.__name__}: Event data not found, skipping...')
                yield None
        except KeyError as e:
            yield None

    def pre_crawl(self, response):
        pass
