import scrapy

from datetime import datetime
from typing import List, Tuple
from ludoparser import SportSelector
from ludoparser.items import Event, Sport, EventData


class InconsistentDataError(Exception):
    pass


class BaseSpider(scrapy.Spider):

    sports: dict = None

    custom_settings = {
        'LOG_LEVEL': 'WARNING'
    }

    def __init__(self, item_defs: list, base_url: str = None, base_event_url: str = None, pre_crawl: bool = False,
                 live: bool = False, **kwargs):
        super().__init__(**kwargs)

        self.start_urls = [base_url.format(item_def) for item_def in item_defs] if base_url else item_defs
        self.pre_crawl_enabled = pre_crawl
        self.event_url = base_event_url
        self.live = live

    def start_requests(self):
        callback = self.pre_crawl if self.pre_crawl_enabled else None
        for url in self.start_urls:
            yield scrapy.Request(url, dont_filter=True, callback=callback)

    def get_sport(self, sport_id):
        if sport_id not in self.sports:
            raise InconsistentDataError

        ludoparser_sport_id = self.sports[sport_id]
        return dict(Sport(id=ludoparser_sport_id, name=SportSelector.get(ludoparser_sport_id)))

    @staticmethod
    def timestamp_to_date_string(timestamp):
        return datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')

    @staticmethod
    def get_status(timestamp):
        return 0 if datetime.now().timestamp() < timestamp else 1

    @staticmethod
    def get_last_update():
        return datetime.now().timestamp()

    def pre_crawl(self, response):
        raise NotImplementedError(f'{self.__class__.name}: Необходимо определить метод пре-кравлинга!')

    def parse(self, response):
        for chunk in self.split_response(response):
            try:
                event, data_link = self.parse_event(chunk)
            except InconsistentDataError:
                return None
            yield event

            data_url = self.event_url.format(data_link) if self.event_url else data_link
            yield scrapy.Request(data_url, dont_filter=True, callback=self.parse_data)

    def split_response(self, response) -> list:
        raise NotImplementedError(f'{self.__class__.name}: Необходимо определить метод разделения ответа!')

    def parse_event(self, chunk) -> Tuple[Event, str]:
        raise NotImplementedError(f'{self.__class__.name}: Необходимо определить метод парсинга события!')

    def parse_data(self, response) -> EventData:
        raise NotImplementedError(f'{self.__class__.name}: Необходимо определить метод парсинга дополнительных данных!')
