BOT_NAME = 'ludoparser'

SPIDER_MODULES = ['ludoparser.spiders']
NEWSPIDER_MODULE = 'ludoparser.spiders'

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0'

ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 65536

COOKIES_ENABLED = True
TELNETCONSOLE_ENABLED = True

LOG_LEVEL = 'INFO'

ITEM_PIPELINES = {
    'ludoparser.pipelines.LudoparserPipeline': 300,
}

RMQ_HOST = 'localhost'
RMQ_PORT = '5672'
RMQ_USERNAME = 'dev'
RMQ_PASSWORD = 'dev'
RMQ_EXCHANGE = 'ludoparser'

CRAWLERA_ENABLED = False
CRAWLERA_APIKEY = '96050fc133bd4ae5bf52cfea76f7183e'
# Если не используется дефолтный URL
# CRAWLERA_URL = ''

DOWNLOADER_MIDDLEWARES = {
    'scrapy_crawlera.CrawleraMiddleware': 610
}

# Дефолтные хедеры, на случай если понадобятся:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Middleware пауков (если понадобятся)
#SPIDER_MIDDLEWARES = {
#    'ludoparser.middlewares.LudoparserSpiderMiddleware': 543,
#}
